﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FlashCardsWeb.Startup))]
namespace FlashCardsWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
