﻿<%@ Page Title="Play" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FlashCards.aspx.cs" Inherits="FlashCardsWeb.Secure.FlashCards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        <asp:Label ID="lblWelcome" runat="server" Text="Welcome ..."></asp:Label>
    </h2>
    <p>
        <asp:Label ID="lblLifetimeStats" runat="server" Text="Label"></asp:Label>
    </p>
    <h3>Do you want to:</h3>
    <p>
        <asp:RadioButton ID="rdoAdd" runat="server" Checked="True" GroupName="Operation" Text="Add" />
    </p>
    <p>
        <asp:RadioButton ID="rdoSubtract" runat="server" GroupName="Operation" Text="Subtract" />
    </p>
    <p>
        <asp:RadioButton ID="rdoMultiply" runat="server" GroupName="Operation" Text="Multiply" />
    </p>
    <p>
        <asp:RadioButton ID="rdoDivide" runat="server" GroupName="Operation" Text="Divide" />
    </p>
    <p>
        <asp:Label ID="lblQuestion" runat="server" Text="99 + 99 =" ClientIDMode="Static"></asp:Label>
        <asp:TextBox ID="txtAnswer" runat="server" CssClass="answer"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="btnPlay" runat="server" Text="Play" />
    </p>
    <p>
        <asp:Label ID="lblAnswer" runat="server" Text="Click the Play button to start"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblScore" runat="server" Text="Score: 0" CssClass="score"></asp:Label>
    </p>
</asp:Content>
