﻿using System;

using FlashCardsLibrary;

namespace FlashCardsWeb.Secure
{
    public partial class FlashCards : System.Web.UI.Page
    {
        private FlashCardsGame game;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FlashCardsGame"] == null)
            {
                game = new FlashCardsGame("A");
                Session["FlashCardsGame"] = game;
            } else
            {
                game = (FlashCardsGame)Session["FlashCardsGame"];
            }

            game.ScoreChanged += game_ScoreChanged;

            if (IsPostBack == false)
            {
                lblQuestion.Text = "";
                game.User = User.Identity.Name;
                lblWelcome.Text = "Hello " + game.User + ".";
                lblLifetimeStats.Text = FlashCardsDataAccess.GetLifetimeStatistics(game.User);
            } else
            {
                FormSubmitted();
            }
        }

        void game_ScoreChanged(object sender, EventArgs e)
        {
            lblScore.Text = string.Format("Score: {0}", game.Score);
        }

        private void FormSubmitted()
        {
            if (btnPlay.Text == "Play")
            {
                if (rdoAdd.Checked)
                {
                    game.Operation = "A";
                } else if (rdoSubtract.Checked)
                {
                    game.Operation = "S";
                } else if (rdoMultiply.Checked)
                {
                    game.Operation = "M";
                } else if (rdoDivide.Checked)
                {
                    game.Operation = "D";
                }

                game.GenerateNumbers();
                lblQuestion.Text = game.BuildEquation();
                btnPlay.Text = "Check Answer";
                txtAnswer.Text = "";
                txtAnswer.Focus();
            } else
            {
                double answer;
                if (double.TryParse(txtAnswer.Text, out answer))
                {
                    if (game.CheckAnswer(answer))
                    {
                        lblAnswer.Text = string.Format
                            ("Correct! Your score is {0} out of {1} for {2} percent."
                            , game.Correct, game.Tries, game.CalculatePercentCorrect());

                        // Add the FlashCard to the database with 10 points
                        FlashCardsDataAccess.SaveFlashCard(game, answer, 10);
                    } else
                    {
                        lblAnswer.Text = string.Format
                            ("Incorrect.  Your score is {0} out of {1} for {2} percent."
                            , game.Correct, game.Tries, game.CalculatePercentCorrect());

                        // Add FlashCard to the database with 0 points
                        FlashCardsDataAccess.SaveFlashCard(game, answer, 0);
                    }
                    btnPlay.Text = "Play";
                } else
                {
                    txtAnswer.Focus();
                }
            }

            // Update lifetime stats
            lblLifetimeStats.Text = FlashCardsDataAccess.GetLifetimeStatistics(game.User);
        }
    }
}