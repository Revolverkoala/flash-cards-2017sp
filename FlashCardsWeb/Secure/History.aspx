﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="FlashCardsWeb.Secure.History" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Your History</h2>
<p>
    <asp:HiddenField ID="userName" runat="server" />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            <asp:BoundField DataField="QuestionTime" HeaderText="QuestionTime" SortExpression="QuestionTime" />
            <asp:BoundField DataField="Equation" HeaderText="Equation" SortExpression="Equation" />
            <asp:BoundField DataField="CorrectAnswer" HeaderText="CorrectAnswer" SortExpression="CorrectAnswer" />
            <asp:BoundField DataField="StudentAnswer" HeaderText="StudentAnswer" SortExpression="StudentAnswer" />
            <asp:BoundField DataField="Points" HeaderText="Points" SortExpression="Points" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:FlashCardsDatabaseConnectionString %>" SelectCommand="SELECT * FROM [Scores] WHERE ([UserName] = @UserName)">
        <SelectParameters>
            <asp:ControlParameter ControlID="userName" Name="UserName" PropertyName="Value" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</p>
</asp:Content>
