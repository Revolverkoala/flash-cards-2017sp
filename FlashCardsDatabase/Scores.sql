﻿CREATE TABLE [dbo].[Scores]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] NVARCHAR(450) NOT NULL, 
    [QuestionTime] DATETIME NULL, 
    [Equation] NVARCHAR(50) NULL, 
    [CorrectAnswer] FLOAT NULL, 
    [StudentAnswer] FLOAT NULL, 
    [Points] INT NULL
)

GO


CREATE INDEX [IX_Scores_UserName] ON [dbo].[Scores] ([UserName])
