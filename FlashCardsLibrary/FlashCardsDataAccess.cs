﻿using System;
using System.Data.Entity;
using System.Configuration;

namespace FlashCardsLibrary
{
    public class FlashCardsDataAccess
    {
        public static Database FlashCardsDatabase
        {
            get
            {
                string connection = ConfigurationManager.ConnectionStrings["FlashCardsDatabaseConnectionString"].ConnectionString;
                return new DbContext(connection).Database;
            }
        }

        public static void SaveFlashCard(FlashCardsGame flashCard, double studentAnswer, int points)
        {
            string sql;
            sql = "INSERT INTO Scores (UserName, QuestionTime, Equation, CorrectAnswer, StudentAnswer, Points) VALUES ({0}, {1}, {2}, {3}, {4}, {5})";
            FlashCardsDatabase.ExecuteSqlCommand(sql, flashCard.User
               , DateTime.Now, flashCard.BuildEquation()
               , flashCard.CalculateCorrectAnswer(), studentAnswer, points);
        }

        public static string GetLifetimeStatistics(string username)
        {
            string sql;
            int questions = 0;
            int correct = 0;
            int points = 0;

            sql = "Select CorrectAnswer, StudentAnswer, Points FROM Scores WHERE UserName = {0}";
            var scores = FlashCardsDatabase.SqlQuery<Score>(sql, username);

            foreach (var score in scores)
            {
                questions += 1;
                if (score.CorrectAnswer == score.StudentAnswer)
                {
                    correct += 1;
                }
                points += score.Points;
            }

            return string.Format("All time, you have {0} correct out of {1} questions for {2} points."
                , correct, questions, points);
        }
    }
}
