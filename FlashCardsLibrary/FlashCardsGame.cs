﻿using System;
using System.Collections.Generic;

namespace FlashCardsLibrary
{
    public class FlashCardsGame
    {
        // constructor
        public FlashCardsGame(string operation)
        {
            Operation = operation;

            var addSubGen = new NumberGenerator(rng);
            _generators.Add("A", addSubGen);
            _generators.Add("S", addSubGen);
            _generators.Add("M", new MultiplicationNumberGenerator(rng));
            _generators.Add("D", new DivisionNumberGenerator(rng));
        }

        // public properties
        public string User { get; set; }
        public double Number1 { get; set; }
        public double Number2 { get; set; }
        public int Correct { get; set; }
        public int Tries { get; set; }
        public string Operation
        {
            get { return _operation; }
            set
            {
                // NOTE: Exceptions are relatively expensive, so I try not to
                // rely on them for routine things like input validation.
                // However, there's no other convenient way to signal that an
                // error occured in e.g. a setter or a constructor

                // SIMPLE EXCEPTION USAGE:
                // Throw an exception if the user entered an empty string or
                // just whitespace
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("You must enter a value for the operation.");
                }

                // Get the first character and convert to uppercase
                string input = value.Substring(0, 1).ToUpper();

                if (input == "A" || input == "S" ||
                    input == "M" || input == "D")
                {
                    _operation = input;
                } else
                {
                    // The input must be among the acceptable options
                    throw new Exception("You must enter Add, Subtract, Multiply, or Divide");
                }
            }
        }

        // New public event and Score property
        public event EventHandler ScoreChanged;
        public int Score { get; set; }

        // public methods
        public void GenerateNumbers()
        {
            NumberGenerator generator = _generators[Operation];

            generator.GenerateNumbers(this);
        }

        public string BuildEquation()
        {
            switch (Operation)
            {
                case "A":
                    return Number1.ToString() + " + " + Number2.ToString();
                case "S":
                    return Number1.ToString() + " - " + Number2.ToString();
                case "M":
                    return Number1.ToString() + " * " + Number2.ToString();
                default:
                    return Number1.ToString() + " / " + Number2.ToString();
            }
        }

        public double CalculateCorrectAnswer()
        {
            if (Operation == "A")
            {
                return Number1 + Number2;
            } else if (Operation == "S")
            {
                return Number1 - Number2;
            } else if (Operation == "M")
            {
                return Number1 * Number2;
            } else
            {
                return Number1 / Number2;
            }
        }

        public double CalculatePercentCorrect()
        {
            return ((double)Correct / Tries) * 100.0;
        }

        public bool CheckAnswer(double answer)
        {
            Tries += 1;

            if (CalculateCorrectAnswer() == answer)
            {
                Correct += 1;
                Score += 10;

                if (ScoreChanged != null)
                {
                    ScoreChanged(this, EventArgs.Empty);
                }

                // Visual Studio says the line after this comment is
                // preferable to the above but it's easy to understand what
                // the one above means
                //ScoreChanged?.Invoke(this, EventArgs.Empty);
                return true;
            } else
            {
                return false;
            }
        }
        // private data members
        private string _operation;
        // random number generator
        private Random rng = new Random();
        private IDictionary<string, NumberGenerator> _generators = new Dictionary<string, NumberGenerator>();
    }
}