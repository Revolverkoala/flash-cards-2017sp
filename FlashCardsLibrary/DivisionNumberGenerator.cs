﻿using System;

namespace FlashCardsLibrary
{
    class DivisionNumberGenerator : NumberGenerator
    {
        public DivisionNumberGenerator(Random rng) : base(rng) { }

        public override void GenerateNumbers(FlashCardsGame flashcard)
        {
            int temp;
            temp = rng.Next(1, 9);
            flashcard.Number2 = rng.Next(1, 12);
            flashcard.Number1 = temp * flashcard.Number2;
        }
    }
}
