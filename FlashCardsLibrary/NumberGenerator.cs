﻿using System;

namespace FlashCardsLibrary
{
    class NumberGenerator
    {
        // Reference to the game's rng
        protected Random rng;

        // constructor
        public NumberGenerator(Random rand)
        {
            rng = rand;
        }

        public virtual void GenerateNumbers(FlashCardsGame flashcard)
        {
            flashcard.Number1 = rng.Next(0, 99);
            flashcard.Number2 = rng.Next(0, 99);
        }
    }
}
