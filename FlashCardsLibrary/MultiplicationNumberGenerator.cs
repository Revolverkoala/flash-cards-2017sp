﻿using System;

namespace FlashCardsLibrary
{
    class MultiplicationNumberGenerator : NumberGenerator
    {
        public MultiplicationNumberGenerator(Random rng) : base(rng) { }

        public override void GenerateNumbers(FlashCardsGame flashcard)
        {
            flashcard.Number1 = rng.Next(1, 12);
            flashcard.Number2 = rng.Next(1, 12);
        }
    }
}
