﻿namespace FlashCardsLibrary
{
    public class Score
    {
        public double CorrectAnswer { get; set; }
        public double StudentAnswer { get; set; }
        public int Points { get; set; }
    }
}
