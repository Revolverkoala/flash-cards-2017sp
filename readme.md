# Flash Card Project

## PLEASE READ CAREFULLY TO AVOID SQL HEARTACHE

The following ONLY applies to parts 5 and 6.

**DO NOT MOVE ANY .mdf OR .ldf FILES BETWEEN COMPUTERS.** Opening a .mdf database in a newer version of SQL Server WILL render it forever incompatible with older versions! This is the *exact* problem that Visual Studio's SQL Server projects are intended to help with.

My general advice: NEVER copy/transfer .mdf/.ldf files between machines, ALWAYS use scripts (e.g. a SQL project in Visual Studio) to create the database on each machine and to seed it with test data if needed. Depending on the needs of your team/project, you could instead use a remote SQL server; like Microsoft Azure, or your company's development server if you have access to one. Entity Framework also has what's called a "code first" workflow that creates database tables based on classes you write; that's what ASP.NET's identity framework uses to automatically create its tables.

Advice for part 6, with the database project:

Unless you change the table layout (what nerds call the "schema") or break something, you should only have to publish your database project once per computer. If you do make a mistake, like if you left out a column or made it the wrong data type, you can just delete the whole FlashCardsDatabase (using SQL Server Object Explorer in Visual Studio) and publish again once you fix your table. It won't hurt anything so do it as many times as you need.

Later in the project you'll add a connection string to your C# code, initially in the file FlashCardsDataAccess.cs, so your program knows how to connect to your custom database. At one point you'll have to choose one of the SQL Server database instances on your machine. There might be only one or there might be several, if your computer has multiple versions of SQL Server installed at the same time. Visual Studio comes with a free version of SQL Server called SQL Express LocalDB, which will be one of the instances you can choose from. Newer versions of LocalDB all call their instance "(LocalDB)\MSSQLLocalDB", which is why I recommend using that in your connection string: since LocalDB comes free with Visual Studio, that string should work with any version that's at least 2013. But we have 2012 at school for now, so you may have to use "(LocalDB)\v11.0" instead. Just remember if you're going between a school computer and one with newer Visual Studio, you'll have to change the string both when you publish the database and in your source code.

## 1.1

Create empty project. Add initial .cs file, set it as startup object. Hello World status

## 1.2

Add FlashCardGame.cs with framework for card game class. Add code to Startup.cs to select operation (read a string from the console into the Operation property, then parrot it back).

## 1.3

FlashCardsGame.cs: Add GenerateNumbers(), BuildEquation(), CalculateCorrectAnswer(), CalculatePercentCorrect()

Startup.cs: Add code to display an equation

## 2.1

FlashCardsGame.cs: Add random number generator, make BuildEquation and GenerateNumbers work; add CheckAnswer method

Startup.cs: Add logic to make addition work; the game now plays a single time, no real input validation

## 2.2

FlashCardsGame.cs: add validation to Operation property, add code to select operation

Startup.cs: Add a response based on whether the user answered correctly

## 2.3

Startup.cs: Add an input validation loop and a main game loop

## 2.4

FlashCardsGame.cs: Add exception-based validation for Operation setter

Startup.cs: Add validation loop for choosing operation

## 3.1

Add a Windows form, FlashCardsForm, and create the interface

## 3.2

FlashCardsForm.cs: Add event handlers for the controls so you can play the game

## 3.3

FlashCardsGame.cs: Add an event and code to fire it when you score points (by getting a question right)

FlashCardsForm.cs: Add an event handler for the new event and a label to show the score

## 3.4

Add UserDialogForm.cs to get the user's name

FlashCardsForm.cs: Add code to show the dialog box and get the user's name; also add a confirmation dialog when closing the form

## 4.1

Add NumberGenerator.cs: Move the random number generation into a separate class

FlashCardsGame.cs: Change GenerateNumbers function to use a NumberGenerator; add constructor to require the operation to be set

FlashCardsForm.cs: Tweak it to work with the new constructor

## 4.2

NumberGenerator.cs: Made GenerateNumbers virtual and the RNG protected to permit better subclassing

Add MultiplicationNumberGenerator.cs, DivisionNumberGenerator.cs: Subclasses of NumberGenerator for better multiplication and division problems

FlashCardsGame.cs: Update GenerateNumbers to use the new generators

## 4.3

Moved FlashCardsGame and related classes into a new class library project, FlashCardsLibrary; changed the namespace to match

FlashCardsForm.cs: Add using declaration for the new library

## 5.1

VS 2015: ASP.NET Web Application (.NET Framework); pick Web Forms, set auth to individual user accounts

Add web forms project, add link to master page, add flash cards form

Web.config: remove "Initial Catalog=" setting from the default connection string. It does nothing but trigger a bug that makes your app crash instead of re-creating the .mdf/.ldf database files if they're missing.

## 5.2

Add code behind for web forms page - now we're able to play the game

## 5.3

Add Web.config to Secure folder to deny access to unauthenticated users; then, add code to show the user's identity

## 5.4

Add ClientIdMode="Static", CssClass="answer", CssClass="score" to FlashCards.aspx
Add to Site.css
Add logo to project and to Site.Master

## 6.1

Create a database project to add to the web game - see accompanying .sql file for schema

Right click solution -> Add new item -> Other Languages -> SQL Server Database Project

Right click DB project -> Add -> Table

You can use the visual designer to create the table, or type the SQL if you're comfortable.

Create a nonclustered, nonunique index on UserName

## 6.2

Install Entity Framework into the library project with NuGet (right click project -> Manage NuGet Packages)

### Publish the DB so your application can connect to it

1. Right click the database project and click Publish.

2. Click Edit... next to Target Database Connection. Use the MSSQLLocalDB connection if available, otherwise use v11.0. VS 2012 may only have the v11.0 instance available. Type a name for the database in the Database Name box; I used FlashCardsDatabase. If you'd like, you can save these settings to a profile so you can load them later; put it in your project folder so it stays together with the code it's meant for. For example, you could have a profile with "(LocalDB)\v11.0" for the school computers and one with "(LocalDB)\MSSQLLocalDB" for your shiny laptop with newer software.

3. Open the SQL Server Object Explorer (in the View menu if it's not on your screen already). Under "SQL Server", open the instance you published your database to and open the Database folder; refresh if you don't see your new one. Right click your new database and click Properties, then copy the entire ConnectionString property (you can click in it and hit ctrl+a to select it all.) Paste it somewhere; you'll need it for the next piece of coding.

Example from my laptop (VS 2015):

Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=FlashCardsDatabase;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False

Add FlashCardsDataAccess.cs - Interface to allow the game to interact with the database

FlashCards.aspx.cs - Add code to use the new functionality

## 6.3

FlashCards.aspx - Add label to show lifetime stats

FlashCards.aspx.cs - Add code to update the label

Library:

FlashCardsDataAccess.cs - add GetLifetimeStatistics method

add Score.cs - data class for DB persistence

### 6.4

add History.aspx - Page to display user's score history

Site.Master - add link to History page

FlashCardsDataAccess.cs - change connection string to the one we saved making the history page (requires System.Configuration reference)