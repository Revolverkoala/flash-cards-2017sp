﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlashCards
{
    class Startup
    {
        public static void Main()
        {
            // Enable visual styles to work with your application; MUST come first
            // This lets the user's OS theme (custom colors, etc) work with your
            // application, if you used system colors
            Application.EnableVisualStyles();

            // This starts the message loop that forms use to handle events
            // (what lets you do the button clicks) and also shows the form
            Application.Run(new FlashCardsForm());

            // This stuff is no longer necessary
            /*
            string input;
            double answer;

            // Say hi
            Console.WriteLine("Welcome to Flashcards!");

            // Initialize game
            FlashCardsGame game = new FlashCardsGame();

            // Get user's name
            Console.WriteLine("What is your name?");
            game.User = Console.ReadLine();

            // Greet user
            Console.WriteLine("Hello, " + game.User);

            // Begin main loop
            do
            {
                // Flag for input validation
                bool validData = false;

                // Begin operation validation loop
                do
                {
                    // Prompt for operation
                    Console.WriteLine
                        ("Do you want to (A)dd, (S)ubtract, (M)ultiply, or (D)ivide?");
                    try
                    {
                        game.Operation = Console.ReadLine();
                        validData = true;
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }
                } while (validData == false);

                Console.WriteLine(game.Operation);

                // Generate numbers
                game.GenerateNumbers();

                validData = false;
                // Begin answer validation loop
                do
                {
                    // Display equation
                    Console.WriteLine(game.BuildEquation());

                    // Get the user's answer and parse

                    input = Console.ReadLine();
                    validData = double.TryParse(input, out answer);

                    if (!validData)
                    {
                        Console.WriteLine("Invalid input, try again.");
                    }
                } while (!validData);

                // Check the answer
                // answer will contain *something*, so we shouldn't crash
                if (game.CheckAnswer(answer))
                {
                    Console.WriteLine("Correct :)");
                }
                else
                {
                    Console.WriteLine("Sorry, wrong answer :(");
                }

                Console.WriteLine("Your score is {0} out of {1} for {2}%",
                    game.Correct, game.Tries, game.CalculatePercentCorrect());

                // Prompt the user whether they want to play again
                Console.Write("Do you want to play again (Y/N)? ");
                input = Console.ReadLine();

            } while (!input.ToUpper().StartsWith("N"));
            // end main loop
            */
        }
    }
}
