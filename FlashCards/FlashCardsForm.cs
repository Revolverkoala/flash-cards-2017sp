﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using FlashCardsLibrary;

namespace FlashCards
{
    public partial class FlashCardsForm : Form
    {
        public FlashCardsForm()
        {
            InitializeComponent();
        }

        private FlashCardsGame game = new FlashCardsGame("A");

        private void FlashCardsForm_Load(object sender, EventArgs e)
        {
            // Show our new dialog box and get the user's name from the public text box
            UserDialogForm userDialog = new FlashCards.UserDialogForm();
            userDialog.ShowDialog();
            game.User = userDialog.UserTextBox.Text;

            lblWelcome.Text = "Hello " + game.User;

            lblQuestion.Text = "";

            // Register our event handler
            game.ScoreChanged += game_ScoreChanged;
        }

        // Event handler for when the score changes
        private void game_ScoreChanged(object sender, EventArgs e)
        {
            lblScore.Text = string.Format("Score: {0}", game.Score);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (btnPlay.Text == "Play")
            {
                // Our form does all our validation for us!
                if (rdoAdd.Checked)
                {
                    game.Operation = "A";
                } else if (rdoSubtract.Checked)
                {
                    game.Operation = "S";
                } else if (rdoMultiply.Checked)
                {
                    game.Operation = "M";
                } else if (rdoDivide.Checked)
                {
                    game.Operation = "D";
                }

                game.GenerateNumbers();
                lblQuestion.Text = game.BuildEquation();
                btnPlay.Text = "Check Answer";
                txtAnswer.Text = "";
                txtAnswer.Focus();
            } else
            {
                double answer;
                if (double.TryParse(txtAnswer.Text, out answer))
                {
                    if (game.CheckAnswer(answer))
                    {
                        lblAnswer.Text = string.Format
                            ("Correct! Your score is {0} out of {1} for {2} percent."
                            , game.Correct, game.Tries, game.CalculatePercentCorrect());
                    } else
                    {
                        lblAnswer.Text = string.Format
                            ("Incorrect Your score is {0} out of {1} for {2} percent."
                            , game.Correct, game.Tries, game.CalculatePercentCorrect());
                    }
                    btnPlay.Text = "Play";
                } else
                {
                    txtAnswer.Focus();
                    txtAnswer.SelectAll();
                }
            }
        }

        private void FlashCardsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Display a dialog box and cancel the event if they say no
            if (MessageBox.Show("Are you sure you want to exit?", "Exit?"
                , MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
